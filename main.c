#include "hmi_command.h"

int main(void)
{
	Command c = HmiCommand_Create();
	Command_Run(c);
	Command_Destroy(c);
	return 0;
}
