#ifndef HMI_COMMAND_H
#define HMI_COMMAND_H

#ifdef __cplusplus
extern "C" {
#endif

#include "command.h"

	Command HmiCommand_Create(void);


#ifdef __cplusplus
}
#endif

#endif
