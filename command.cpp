#include "command.hpp"
#include "command.h"

#include <iostream>

namespace Impl {
	Command::~Command() = default;
}

void Command_Destroy(Command self)
{
	if (self)
	{
		if (self->command)
			delete self->command;
		delete self;
	}
}

void Command_Run(Command self)
{
	if (self && self->command)
		self->command->Run();
}
