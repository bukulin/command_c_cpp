#ifndef COMMAND_H
#define COMMAND_H

#ifdef __cplusplus
extern "C" {
#endif

	struct CommandStruct;
	typedef struct CommandStruct CommandStruct;
	typedef CommandStruct* Command;

	void Command_Destroy(Command self);
	void Command_Run(Command self);


#ifdef __cplusplus
}
#endif

#endif
