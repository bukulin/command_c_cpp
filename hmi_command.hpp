#ifndef HMI_COMMAND_HPP
#define HMI_COMMAND_HPP

#include "command.hpp"

namespace Impl
{
	class HmiCommand final : public Command
	{
	public:
		void Run() override;
	};
}


#endif
