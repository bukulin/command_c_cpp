#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <memory>

namespace Impl
{
	class Command
	{
	public:
		virtual ~Command();

	public:
		virtual void Run() = 0;
	};
}

extern "C"
{
	struct CommandStruct
	{
		Impl::Command* command;
	};
}

#endif
