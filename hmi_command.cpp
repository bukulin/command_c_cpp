#include "hmi_command.hpp"
#include "hmi_command.h"

#include <iostream>

namespace Impl {
	void HmiCommand::Run()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
}

Command HmiCommand_Create(void)
{
	auto self = new ::CommandStruct;
	self->command = new Impl::HmiCommand;

	return self;
}
